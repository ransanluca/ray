{
  description = "A very basic flake";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    mozilla = {
      url = "github:mozilla/nixpkgs-mozilla";
      flake = false;
    };
  };

  outputs = { self, nixpkgs, flake-utils, mozilla }:
    flake-utils.lib.eachDefaultSystem (system: let
      rustOverlay = final: prev: let
        rustChannel = prev.rustChannelOf {
          channel = "1.51";
          sha256 = "+EFKtTDUlFY0aUXdSvrz7tAhf5/GsqyVOp8skXGTEJM=";
        };
      in {
        inherit rustChannel;
        rustc = rustChannel.rust;
        cargo = rustChannel.rust;
      };

      pkgs = import nixpkgs {
        inherit system;
        overlays = [
          (import "${mozilla}/rust-overlay.nix")
          rustOverlay
        ];
      };
    in {
      defaultPackage = with pkgs; (makeRustPlatform {
        rustc = rustChannel.rust;
        cargo = rustChannel.rust;
      }).buildRustPackage {
        name = "ray";
        src = self;
        cargoSha256 = "AOl/PFd88exX/Oq/5rZV64AD+5rWFxvllzvSiKq+h7Q=";
        nativeBuildInputs = [
          pkg-config
          wrapGAppsHook
        ];
        buildInputs = [
          gtk3
        ];
      };

      devShell = pkgs.mkShell {
        nativeBuildInputs = with pkgs; [
          rustChannel.rust
          pkg-config
          openssl
          gtk3
        ];

        # rust-analyzer is broken with 1.50
        shellHook = ''
          PATH="${pkgs.lib.makeBinPath [pkgs.rust-analyzer]}:$PATH"
        '';
      };
    });
}
