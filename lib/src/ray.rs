use std::sync::Arc;

use crate::{
    vec::{Vec3, Point3, Color},
    hittable::Hittable,
};

#[derive(Debug, Clone)]
pub struct Ray {
    pub origin: Point3,
    pub direction: Vec3,
}

impl Ray {
    #[inline]
    pub fn new(origin: Point3, direction: Vec3) -> Self {
        Self {
            origin,
            direction,
        }
    }

    #[inline]
    pub fn at(&self, t: f32) -> Vec3 {
        self.origin + t * self.direction
    }

    pub fn color(&self, world: Arc<dyn Hittable>, depth: usize) -> Color {
        if depth == 0 {
            Color::new(0.0, 0.0, 0.0)
        } else if let Some(hr) = world.hit(self, 0.001, f32::INFINITY) {
            if let Some((r, c)) = hr.material.scatter(self, &hr) {
                c * r.color(world, depth - 1)
            } else {
                Color::new(0.0, 0.0, 0.0)
            }
        } else {
            let unit_direction = self.direction.normalized();
            let t = (unit_direction.y() + 1.0) / 2.0;
            (1.0 - t) * Color::new(1.0, 1.0, 1.0) + t * Color::new(0.5, 0.7, 1.0)
        }
    }
}
