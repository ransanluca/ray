use rand::{
    distributions::Uniform,
    thread_rng,
    Rng,
};
use serde::Deserialize;
use std::ops::{
    Add,
    AddAssign,
    Div,
    DivAssign,
    Mul,
    MulAssign,
    Neg,
    Sub,
    SubAssign,
};

#[derive(Debug, Default, Clone, Copy, PartialEq, Deserialize)]
pub struct Vec3 {
    x: f32,
    y: f32,
    z: f32,
}

pub type Point3 = Vec3;
pub type Color = Vec3;

macro_rules! impl_op {
    ($tr:ident, $f:ident, $tra:ident, $fa:ident) => {
        impl $tr for Vec3 {
            type Output = Self;

            #[inline]
            fn $f(self, rhs: Self) -> Self::Output {
                Self {
                    x: $tr::$f(self.x, rhs.x),
                    y: $tr::$f(self.y, rhs.y),
                    z: $tr::$f(self.z, rhs.z),
                }
            }
        }

        impl $tr<f32> for Vec3 {
            type Output = Self;

            #[inline]
            fn $f(self, rhs: f32) -> Self::Output {
                Self {
                    x: $tr::$f(self.x, rhs),
                    y: $tr::$f(self.y, rhs),
                    z: $tr::$f(self.z, rhs),
                }
            }
        }

        impl $tr<Vec3> for f32 {
            type Output = Vec3;

            #[inline]
            fn $f(self, rhs: Vec3) -> Self::Output {
                Vec3 {
                    x: $tr::$f(self, rhs.x),
                    y: $tr::$f(self, rhs.y),
                    z: $tr::$f(self, rhs.z),
                }
            }
        }

        impl $tra for Vec3 {
            #[inline]
            fn $fa(&mut self, rhs: Self) {
                $tra::$fa(&mut self.x, rhs.x);
                $tra::$fa(&mut self.y, rhs.y);
                $tra::$fa(&mut self.z, rhs.z);
            }
        }

        impl $tra<f32> for Vec3 {
            #[inline]
            fn $fa(&mut self, rhs: f32) {
                $tra::$fa(&mut self.x, rhs);
                $tra::$fa(&mut self.y, rhs);
                $tra::$fa(&mut self.z, rhs);
            }
        }
    };
}

impl_op!(Add, add, AddAssign, add_assign);
impl_op!(Sub, sub, SubAssign, sub_assign);
impl_op!(Mul, mul, MulAssign, mul_assign);
impl_op!(Div, div, DivAssign, div_assign);

impl Neg for Vec3 {
    type Output = Self;

    #[inline]
    fn neg(self) -> Self::Output {
        Self {
            x: -self.x,
            y: -self.y,
            z: -self.z,
        }
    }
}

impl Vec3 {
    #[inline]
    pub fn x(&self) -> f32 {
        self.x
    }

    #[inline]
    pub fn y(&self) -> f32 {
        self.y
    }

    #[inline]
    pub fn z(&self) -> f32 {
        self.z
    }

    #[inline]
    pub fn new(x: f32, y: f32, z: f32) -> Self {
        Self {
            x,
            y,
            z,
        }
    }

    #[inline]
    pub fn norm_squared(self) -> f32 {
        self.x * self.x + self.y * self.y + self.z * self.z
    }

    #[inline]
    pub fn norm(self) -> f32 {
        self.norm_squared().sqrt()
    }

    #[inline]
    pub fn dot(self, other: Self) -> f32 {
        self.x * other.x + self.y * other.y + self.z * other.z
    }

    #[inline]
    pub fn cross(self, other: Self) -> Self {
        Self {
            x: self.y * other.z - self.z * other.y,
            y: self.z * other.x - self.x * other.z,
            z: self.x * other.y - self.y * other.x,
        }
    }

    #[inline]
    pub fn normalized(self) -> Self {
        self / self.norm()
    }

    #[inline]
    pub fn reflect(self, normal: Vec3) -> Self {
        self - 2.0 * self.dot(normal) * normal
    }

    #[inline]
    pub fn refract(self, normal: Vec3, n: f32) -> Self {
        let cos_theta = (-self).dot(normal).min(1.0);
        let r_out_perp = n * (self + cos_theta * normal);
        let r_out_parallel = -(1.0 - r_out_perp.norm_squared()).abs().sqrt() * normal;
        return r_out_perp + r_out_parallel;
    }

    #[inline]
    pub fn random() -> Self {
        let mut rng = thread_rng();
        Self {
            x: rng.gen(),
            y: rng.gen(),
            z: rng.gen(),
        }
    }

    #[inline]
    pub fn random_between(min: f32, max: f32) -> Self {
        let mut rng = thread_rng();
        let dist = Uniform::new(min, max);
        Self {
            x: rng.sample(dist),
            y: rng.sample(dist),
            z: rng.sample(dist),
        }
    }

    #[inline]
    pub fn random_in_unit_sphere() -> Self {
        let mut rng = thread_rng();
        let dist = Uniform::new(-1.0, 1.0);
        loop {
            let u = Self {
                x: rng.sample(dist),
                y: rng.sample(dist),
                z: rng.sample(dist),
            };
            if u.norm_squared() < 1.0 {
                return u;
            }
        }
    }

    #[inline]
    pub fn random_in_unit_disk() -> Self {
        let mut rng = thread_rng();
        let dist = Uniform::new(-1.0, 1.0);
        loop {
            let u = Self {
                x: rng.sample(dist),
                y: rng.sample(dist),
                z: 0.0,
            };
            if u.norm_squared() < 1.0 {
                return u;
            }
        }
    }

    #[inline]
    pub fn random_unit_vector() -> Self {
        Self::random_in_unit_sphere().normalized()
    }

    #[inline]
    pub fn random_in_hemisphere(normal: Vec3) -> Self {
        let in_unit_sphere = Self::random_in_unit_sphere();
        if in_unit_sphere.dot(normal) > 0.0 {
            in_unit_sphere
        } else {
            -in_unit_sphere
        }
    }
}
