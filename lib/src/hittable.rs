use serde::{
    de::{SeqAccess, Visitor},
    Deserialize,
    Deserializer,
};
use std::{
    f32::consts::PI,
    fmt,
    sync::Arc,
    vec::Vec,
};

use crate::{
    vec::{Vec3, Point3},
    ray::Ray,
    material::{Material, de_material},
};

#[derive(Clone)]
pub struct HitRecord {
    pub point: Point3,
    pub normal: Vec3,
    pub material: Arc<dyn Material>,
    pub t: f32,
    pub uv: (f32, f32),
    pub front_face: bool,
}

pub trait Hittable: Send + Sync {
    fn hit(&self, ray: &Ray, t_min: f32, t_max: f32) -> Option<HitRecord>;
}

#[derive(Deserialize)]
#[serde(rename = "Hittable")]
enum EnumHittable {
    HittableList(HittableList),
    Sphere(Sphere),
    Triangle(Triangle),
}

impl From<EnumHittable> for Arc<dyn Hittable> {
    fn from(h: EnumHittable) -> Self {
        use EnumHittable::*;
        match h {
            HittableList(hl) => Arc::new(hl),
            Sphere(s) => Arc::new(s),
            Triangle(s) => Arc::new(s),
        }
    }
}

pub(crate) fn de_hittable<'de, D>(de: D) -> Result<Arc<dyn Hittable>, D::Error>
where
    D: Deserializer<'de>,
{
    let h: EnumHittable = Deserialize::deserialize(de)?;
    Ok(h.into())
}

pub(crate) fn de_vec_hittable<'de, D>(de: D) -> Result<Vec<Arc<dyn Hittable>>, D::Error>
where
    D: Deserializer<'de>,
{
    struct HittableVisitor;

    impl<'de> Visitor<'de> for HittableVisitor {
        type Value = Vec<Arc<dyn Hittable>>;

        fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
            formatter.write_str("a sequence of hittables")
        }

        fn visit_seq<S>(self, mut seq: S) -> Result<Self::Value, S::Error>
        where
            S: SeqAccess<'de>,
        {
            let mut hs = Vec::new();
            while let Some(h) = seq.next_element::<EnumHittable>()? {
                hs.push(h.into());
            }
            Ok(hs)
        }
    }

    de.deserialize_seq(HittableVisitor)
}

#[derive(Clone, Deserialize)]
pub struct Sphere {
    pub center: Point3,
    pub radius: f32,
    #[serde(deserialize_with = "de_material")]
    pub material: Arc<dyn Material>,
}

impl Sphere {
    fn get_uv(&self, p: Point3) -> (f32, f32) {
        let theta = (-p.y()).acos();
        let phi = (-p.z()).atan2(p.x()) + PI;
        (phi / (2.0 * PI), theta / PI)
    }
}

impl Hittable for Sphere {
    fn hit(&self, ray: &Ray, t_min: f32, t_max: f32) -> Option<HitRecord> {
        let oc = ray.origin - self.center;
        let a = ray.direction.norm_squared();
        let half_b = oc.dot(ray.direction);
        let c = oc.norm_squared() - self.radius * self.radius;

        let quarter_discriminant = half_b * half_b - a * c;
        if quarter_discriminant < 0.0 {
            return None;
        }

        let hsqrtd = quarter_discriminant.sqrt();

        let mut root = (-half_b - hsqrtd) / a;
        if !(t_min..=t_max).contains(&root) {
            root = (-half_b + hsqrtd) / a;
            if !(t_min..=t_max).contains(&root) {
                return None;
            }
        }

        let point = ray.at(root);
        let outward_normal = (point - self.center) / self.radius;
        let front_face = ray.direction.dot(outward_normal) < 0.0;
        let uv = self.get_uv(outward_normal);

        Some(HitRecord {
            point,
            normal: if front_face {
                outward_normal
            } else {
                -outward_normal
            },
            material: self.material.clone(),
            t: root,
            uv,
            front_face,
        })
    }
}

#[derive(Clone, Deserialize)]
pub struct Triangle {
    pub points: [Point3; 3],
    #[serde(deserialize_with = "de_material")]
    pub material: Arc<dyn Material>,
}

impl Triangle {
    fn get_bary_coef(&self, p: Point3) -> [f32; 3] {
        let bary_a = ((self.points[1].y() - self.points[2].y()) * (p.x() - self.points[2].x()) + (self.points[2].x() - self.points[1].x()) * (p.y() - self.points[2].y())) / ((self.points[1].y() - self.points[2].y()) * (self.points[0].x() - self.points[2].x()) + (self.points[2].x() - self.points[1].x()) * (self.points[1].y() - self.points[2].y()));
        let bary_b = ((self.points[2].y() - self.points[0].y()) * (p.x() - self.points[2].x()) + (self.points[0].x() - self.points[2].x()) * (p.y() - self.points[2].y())) / ((self.points[1].y() - self.points[2].y()) * (self.points[0].x() - self.points[2].x()) + (self.points[2].x() - self.points[1].x()) * (self.points[1].y() - self.points[2].y()));
        let bary_c = 1. - bary_a - bary_b;
        return [bary_a, bary_b, bary_c]
    }

    fn get_uv(&self, p: Point3) -> (f32, f32) {
        let coef = self.get_bary_coef(p);
        let p_uv = coef[0] * Point3::new(1., 0., 0.) + coef[1] * Point3::new(0., 1., 0.) + coef[2] * Point3::new(0., 0., 1.);
        (p_uv.x(), p_uv.y())
    }
}

impl Hittable for Triangle {
    fn hit(&self, ray: &Ray, t_min: f32, t_max: f32) -> Option<HitRecord> {
        //! # Check if the ray intersect with the triangle
        //! We have our triangle ABC, where:
        //! - A is `self.points[0]`
        //! - B is `self.points[1]`
        //! - C is `self.points[2]`
        //!
        //! We define the plane (A, u, v), where:
        //! - u is vector AB
        //! - v is vector AC
        //!
        //! We compute the vector AP, where P is the origin of the ray: `ray.origin`
        //! And we compute the normal of the triangle ABC with u ^ v
        //! We compute the determinant d for put our coordinates relative to the triangle
        //!
        //! Then, we finally compute the coordinates of ray intersection with the plane (A, u, v) and the time where the ray hit the rectangle:
        //! - a = (w ^ v) . direction / d
        //! - b = - (u ^ w) . direction / d
        //! - t = normal . w / d
        //!
        //! Where direction is the direction of the ray: `ray.direction`
        //!
        //! Finally we check if the intersection is in the triangle, ie:
        //! - a > 0
        //! - b > 0
        //! - a + b <= 1
        //!
        //! And if our t is lesser than t_max

        // We define the plane (A, u, v) where u is vector AB and v vector AC
        let u = self.points[1] - self.points[0]; // Vector AB
        let v = self.points[2] - self.points[0]; // Vector AC
        let w = ray.origin - self.points[0]; // Vector AP where P is the origin of the ray
        let normal = u.cross(v); // The normal of the triangle (u ^ v)
        let d = (normal).dot(ray.direction); // Determinant

        let a = (w.cross(v)).dot(ray.direction) / d; // Coordinates of ray intersection with the
        let b = -(w.cross(u)).dot(ray.direction) / d; // plane (A, u, v) relative to the triangle
        //let t = (normal.dot(ray.origin) + normal.dot(self.points[0]))/normal.dot(ray.direction);//w.norm();//(normal).dot(w) / d; // The time where the ray hit the triangle

        let f = 1. / (u.dot(ray.direction.cross(v)));
        let q = (ray.origin - self.points[0]).cross(u);
        let t = f * v.dot(q);

        let p = self.points[0] + b * v + a * u;
        let uv = self.get_uv(p);

        let coef = self.get_bary_coef(p);
        let pc = coef[0] * self.points[0] + coef[1] * self.points[1] + coef[2] * self.points[2];

        if (a > 0.0) && (b > 0.0) && (a + b <= 1.0) && (t <= t_max) && (t >= t_min) {
            // Check if it's inner
            return Some(HitRecord {
                point: ray.at(t),
                normal,
                material: self.material.clone(),
                t/*: (pc - ray.origin).norm()*/,
                uv,
                front_face: true,
            });
        }
        None
    }
}

#[derive(Clone, Deserialize)]
#[serde(transparent)]
pub struct HittableList {
    #[serde(deserialize_with = "de_vec_hittable")]
    pub objects: Vec<Arc<dyn Hittable>>,
}

impl Hittable for HittableList {
    fn hit(&self, ray: &Ray, t_min: f32, mut t_max: f32) -> Option<HitRecord> {
        let mut hr: Option<HitRecord> = None;
        for o in &self.objects {
            if let Some(h) = o.hit(ray, t_min, t_max) {
                t_max = h.t;
                hr = Some(h);
            }
        }
        return hr;
    }
}

impl HittableList {
    pub fn new() -> Self {
        HittableList {
            objects: Vec::new(),
        }
    }

    pub fn push<H: Hittable + 'static>(&mut self, hittable: H) {
        self.objects.push(Arc::new(hittable));
    }
}
