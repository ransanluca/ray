use std::sync::Arc;
use serde::Deserialize;

use crate::{
    camera::Camera,
    hittable::{Hittable, de_hittable},
};

#[derive(Deserialize)]
pub struct Scene {
    pub camera: Camera,
    #[serde(deserialize_with = "de_hittable")]
    pub world: Arc<dyn Hittable>,
}
