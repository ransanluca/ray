use std::sync::Arc;
use serde::{
    Deserialize,
    Deserializer,
};

use crate::{
    vec::{Vec3, Color},
    hittable::HitRecord,
    ray::Ray,
    texture::{Texture, de_texture},
};

pub trait Material: Send + Sync {
    fn scatter(&self, ray: &Ray, hit_record: &HitRecord) -> Option<(Ray, Color)>;
}

pub(crate) fn de_material<'de, D>(de: D) -> Result<Arc<dyn Material>, D::Error>
where
    D: Deserializer<'de>,
{
    #[derive(Deserialize)]
    #[serde(rename = "Material")]
    enum EnumMaterial {
        Lambertian(Lambertian),
        Metal(Metal),
        Dielectric(Dielectric),
    }

    impl From<EnumMaterial> for Arc<dyn Material> {
        fn from(m: EnumMaterial) -> Self {
            use EnumMaterial::*;
            match m {
                Lambertian(l) => Arc::new(l),
                Metal(me) => Arc::new(me),
                Dielectric(d) => Arc::new(d),
            }
        }
    }

    let m: EnumMaterial = Deserialize::deserialize(de)?;
    Ok(m.into())
}

#[derive(Clone, Deserialize)]
pub struct Lambertian {
    #[serde(deserialize_with = "de_texture")]
    pub albedo: Arc<dyn Texture>,
}

impl Material for Lambertian {
    fn scatter(&self, _ray: &Ray, hit_record: &HitRecord) -> Option<(Ray, Color)> {
        let scatter_direction = hit_record.normal + Vec3::random_unit_vector();
        Some((
            Ray::new(hit_record.point, scatter_direction),
            self.albedo.value(hit_record.uv, &hit_record.point),
        ))
    }
}

#[derive(Clone, Deserialize)]
pub struct Metal {
    #[serde(deserialize_with = "de_texture")]
    pub albedo: Arc<dyn Texture>,
    pub fuzz: f32,
}

impl Material for Metal {
    fn scatter(&self, ray: &Ray, hit_record: &HitRecord) -> Option<(Ray, Color)> {
        let reflected = ray.direction.normalized().reflect(hit_record.normal);
        let scattered = Ray::new(
            hit_record.point,
            reflected + self.fuzz * Vec3::random_in_unit_sphere(),
        );
        if scattered.direction.dot(hit_record.normal) > 0.0 {
            Some((
                scattered,
                self.albedo.value(hit_record.uv, &hit_record.point),
            ))
        } else {
            None
        }
    }
}

#[derive(Debug, Clone, Deserialize)]
pub struct Dielectric {
    pub ir: f32,
}

impl Material for Dielectric {
    fn scatter(&self, ray: &Ray, hit_record: &HitRecord) -> Option<(Ray, Color)> {
        let reflection_ratio = if hit_record.front_face {
            1.0 / self.ir
        } else {
            self.ir
        };

        let unit_direction = ray.direction.normalized();
        let cos_theta = (-unit_direction).dot(hit_record.normal).min(1.0);
        let sin_theta = (1.0 - cos_theta * cos_theta).sqrt();

        let direction = if reflection_ratio * sin_theta > 1.0
            || reflectance(cos_theta, reflection_ratio) > rand::random()
        {
            unit_direction.reflect(hit_record.normal)
        } else {
            unit_direction.refract(hit_record.normal, reflection_ratio)
        };

        Some((
            Ray::new(hit_record.point, direction),
            Color::new(1.0, 1.0, 1.0),
        ))
    }
}

#[inline]
fn reflectance(cosine: f32, ref_idx: f32) -> f32 {
    let sqrt_r0 = (1.0 - ref_idx) / (1.0 + ref_idx);
    let r0 = sqrt_r0 * sqrt_r0;
    return r0 + (1.0 - r0) * (1.0 - cosine).powi(5);
}
