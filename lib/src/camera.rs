use serde::Deserialize;

use crate::{
    vec::{Vec3, Point3},
    ray::Ray,
};

#[derive(Deserialize, Clone)]
pub struct CamParameters {
    pub lookfrom: Point3,
    pub dir: Vec3,
    pub vup: Vec3,
    pub vfov: f32,
    pub aspect_ratio: f32,
    pub aperture: f32,
    pub focus_dist: f32,
}

#[derive(Deserialize)]
#[serde(from = "CamParameters")]
pub struct Camera {
    pub origin: Point3,
    pub lower_left_corner: Point3,
    pub horizontal: Vec3,
    pub vertical: Vec3,
    pub u: Vec3,
    pub v: Vec3,
    pub w: Vec3,
    pub lens_radius: f32,
    pub aspect_ratio: f32,
    pub parameters: CamParameters,
}

impl Camera {
    pub fn new(cp: CamParameters) -> Self {
        let theta = cp.vfov.to_radians();
        let h = (theta / 2.0).tan();
        let viewport_height = h * 2.0;
        let viewport_width = cp.aspect_ratio * viewport_height;

        let w = -cp.dir.normalized();
        let u = cp.vup.cross(w).normalized();
        let v = w.cross(u);

        let origin = cp.lookfrom;
        let horizontal = cp.focus_dist * viewport_width * u;
        let vertical = cp.focus_dist * viewport_height * v;
        let lower_left_corner = origin - horizontal / 2.0 - vertical / 2.0 - cp.focus_dist * w;

        let lens_radius = cp.aperture / 2.0;

        Self {
            origin,
            lower_left_corner,
            horizontal,
            vertical,
            u,
            v,
            w,
            lens_radius,
            aspect_ratio: cp.aspect_ratio,
            parameters: cp,
        }
    }

    #[inline]
    pub fn get_ray(&self, s: f32, t: f32) -> Ray {
        let rd = self.lens_radius * Vec3::random_in_unit_disk();
        let offset = self.u * rd.x() + self.v * rd.y();

        Ray {
            origin: self.origin + offset,
            direction: self.lower_left_corner + s * self.horizontal + t * self.vertical
                - self.origin
                - offset,
        }
    }
}

impl From<CamParameters> for Camera {
    fn from(c: CamParameters) -> Self {
        Self::new(c)
    }
}
