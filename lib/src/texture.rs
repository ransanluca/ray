use std::sync::Arc;
use serde::{
    Deserialize,
    Deserializer,
    de::Error,
};
use image::{
    RgbImage,
    io::Reader,
};

use crate::vec::{Point3, Color};

pub trait Texture: Send + Sync {
    fn value(&self, uv: (f32, f32), p: &Point3) -> Color;
}

pub(crate) fn de_texture<'de, D>(de: D) -> Result<Arc<dyn Texture>, D::Error>
where
    D: Deserializer<'de>,
{
    #[derive(Deserialize)]
    enum EnumTexture {
        SolidColor(SolidColor),
        CheckerTexture(CheckerTexture),
        ImageTexture(ImageTexture),
    }

    impl From<EnumTexture> for Arc<dyn Texture> {
        fn from(t: EnumTexture) -> Self {
            use EnumTexture::*;
            match t {
                SolidColor(c) => Arc::new(c),
                CheckerTexture(c) => Arc::new(c),
                ImageTexture(i) => Arc::new(i),
            }
        }
    }

    let t: EnumTexture = Deserialize::deserialize(de)?;
    Ok(t.into())
}

#[derive(Clone, Deserialize)]
#[serde(transparent)]
pub struct SolidColor {
    pub color: Color,
}

impl Texture for SolidColor {
    fn value(&self, _uv: (f32, f32), _p: &Point3) -> Color {
        self.color
    }
}

#[derive(Clone, Deserialize)]
pub struct CheckerTexture {
    #[serde(deserialize_with = "de_texture")]
    pub even: Arc<dyn Texture>,
    #[serde(deserialize_with = "de_texture")]
    pub odd: Arc<dyn Texture>,
    pub size: f32,
}

impl Texture for CheckerTexture {
    fn value(&self, uv: (f32, f32), p: &Point3) -> Color {
        let sines = (self.size * p.x()).sin() * (self.size * p.y()).sin() * (self.size * p.z()).sin();
        if sines < 0.0 {
            self.odd.value(uv, p)
        } else {
            self.even.value(uv, p)
        }
    }
}

#[derive(Clone, Deserialize)]
#[serde(transparent)]
pub struct ImageTexture {
    #[serde(deserialize_with = "de_image")]
    pub image: Arc<RgbImage>,
}

impl Texture for ImageTexture {
    fn value(&self, uv: (f32, f32), _p: &Point3) -> Color {
        let w = self.image.width();
        let h = self.image.height();
        let i = (uv.0.clamp(0.0, 1.0) * w as f32) as u32;
        let j = ((1.0 - uv.1.clamp(0.0, 1.0)) * h as f32) as u32;
        let scale = 1.0 / 255.0;
        let pixel = self.image[(
            if i < w { i } else { w - 1 },
            if j < h { j } else { h - 1 },
        )];
        Color::new(
            scale * pixel[0] as f32,
            scale * pixel[1] as f32,
            scale * pixel[2] as f32,
        )
    }
}

pub(crate) fn de_image<'de, D>(de: D) -> Result<Arc<RgbImage>, D::Error>
where
    D: Deserializer<'de>,
{
    let file: String = Deserialize::deserialize(de).map_err(D::Error::custom)?;
    let image: RgbImage = Reader::open(file)
        .map_err(D::Error::custom)?
        .decode()
        .map_err(D::Error::custom)?
        .to_rgb8();
    Ok(Arc::new(image))
}
