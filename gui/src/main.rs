use anyhow::{
    Context,
    Result,
};
use clap::{
    App,
    Arg,
};
use rand::prelude::*;
use std::{
    convert::TryInto,
    sync::Arc,
    thread,
};
use gtk::prelude::*;
use gio::prelude::*;
use gdk::{
    prelude::*,
    EventMask,
    EventType,
};
use glib::{
    translate::*,
    clone,
};
// std's one starves the writers
use parking_lot::RwLock;

use libray::{
    vec::Color,
    camera::CamParameters,
    scene::Scene,
};

#[derive(Clone)]
struct Image {
    buf: Arc<Vec<RwLock<Vec<(Color, usize)>>>>,
    width: usize,
    height: usize,
}

fn main() -> Result<()> {
    let matches = App::new(clap::crate_name!())
        .version(clap::crate_version!())
        .author(clap::crate_authors!())
        .about(clap::crate_description!())
        .arg(
            Arg::with_name("SCENE")
                .help("The YAML scene file")
                .required(true),
        )
        .arg(
            Arg::with_name("output_file")
                .short("o")
                .long("output")
                .takes_value(true)
                .value_name("PATH")
                .help("The output image"),
        )
        .arg(
            Arg::with_name("image_height")
                .short("h")
                .long("height")
                .takes_value(true)
                .value_name("HEIGHT")
                .default_value("360")
                .help("The height in pixels of the image"),
        )
        .arg(
            Arg::with_name("max_depth")
                .short("d")
                .long("depth")
                .takes_value(true)
                .value_name("DEPTH")
                .default_value("50")
                .help("The number of times a ray can bounce"),
        )
        .get_matches();

    let image_height: usize = matches.value_of("image_height").unwrap().parse()?;
    let max_depth: usize = matches.value_of("max_depth").unwrap().parse()?;

    let scene_file = matches.value_of("SCENE").unwrap();
    let scene_contents = std::fs::read_to_string(scene_file)?;
    let scene: Arc<RwLock<Scene>> = Arc::new(RwLock::new(
        serde_yaml::from_str(&scene_contents)
            .with_context(|| format!("Failed to read scene from {}", scene_file))?
    ));

    let image_width = (image_height as f32 * scene.read().camera.aspect_ratio) as usize;

    let img = Image::new(image_width, image_height);

    for _ in 0..16 {
        let i = img.clone();
        let s = scene.clone();
        thread::spawn(move || thread_work(i, s, max_depth));
    }

    let application = gtk::Application::new(
        Some("fr.eisti.etude.gitlab.ransanluca.ray"),
        Default::default(),
    ).expect("failed to initialize GTK application");

    application.connect_activate(move |app| {
        let window = gtk::ApplicationWindow::new(app);
        window.set_title("Test");

        let drawing_area = gtk::DrawingArea::new();
        drawing_area.set_size_request(image_width as i32, image_height as i32);
        drawing_area.set_can_focus(true);
        drawing_area.add_events(
            EventMask::BUTTON1_MOTION_MASK
                | EventMask::KEY_PRESS_MASK
                | EventMask::SCROLL_MASK
        );

        drawing_area.connect_event(
            clone!(@strong scene, @strong img =>
                   move |w, e| movement(w, e, scene.clone(), img.clone()))
        );
        drawing_area.connect_draw(
            clone!(@strong img =>
                   move |w, c| draw_area(w, c, img.clone()))
        );
        drawing_area.add_tick_callback(move |w, _| {
            w.queue_draw();
            Continue(true)
        });

        unsafe {
            gtk_sys::gtk_widget_set_double_buffered(
                drawing_area.clone().upcast::<gtk::Widget>().to_glib_none().0,
                false.to_glib(),
            );
        }
        
        window.add(&drawing_area);

        window.show_all();
    });

    application.run(&[]);

    Ok(())
}

fn draw_area(
    _widget: &gtk::DrawingArea,
    cr: &cairo::Context,
    img: Image,
) -> Inhibit {
    let mut surface: cairo::ImageSurface = cr.get_target()
        .create_similar_image(cairo::Format::Rgb24, img.width as i32, img.height as i32)
        .unwrap().try_into().unwrap();

    let stride = surface.get_stride();
    {
        let mut data = surface.get_data().unwrap();

        for (y, m) in img.buf.iter().enumerate() {
            let lock = m.read();
            for (x, (c, s)) in lock.iter().enumerate() {
                let p = (*c * 256.0) / *s as f32;
                let i = y * stride as usize + x * 4;

                if cfg!(target_endian = "little") {
                    data[i + 0] = p.z() as u8;
                    data[i + 1] = p.y() as u8;
                    data[i + 2] = p.x() as u8;
                } else {
                    data[i + 1] = p.x() as u8;
                    data[i + 2] = p.y() as u8;
                    data[i + 3] = p.z() as u8;
                }
            }
        }
    }

    surface.mark_dirty();
    cr.set_source_surface(&surface, 0.0, 0.0);
    cr.paint();

    Inhibit(false)
}

fn movement(
    _widget: &gtk::DrawingArea,
    event: &gdk::Event,
    scene: Arc<RwLock<Scene>>,
    img: Image,
) -> Inhibit {
    use EventType::*;

    #[inline(always)]
    fn modify<F>(scene: Arc<RwLock<Scene>>, img: Image, f: F) -> Inhibit
    where
        F: Fn(&mut CamParameters)
    {
        let mut lock = scene.write();
        let mut params = lock.camera.parameters.clone();
        f(&mut params);
        lock.camera = params.into();
        img.clear();
        Inhibit(true)
    }

    match event.get_event_type() {
        Scroll => {
            use gdk::ScrollDirection::*;
            match event.get_scroll_direction() {
                Some(Up) => modify(scene, img, |params| {
                    params.focus_dist += 1.0;
                }),
                Some(Down) => modify(scene, img, |params| {
                    params.focus_dist -= 1.0;
                }),
                _ => Inhibit(false)
            }
        },

        KeyPress => {
            use gdk::keys::constants;
            match event.downcast_ref::<gdk::EventKey>().unwrap().get_keyval() {
                constants::Up => modify(scene, img, |params| {
                    params.lookfrom += params.dir.normalized();
                }),
                constants::Down => modify(scene, img, |params| {
                    params.lookfrom -= params.dir.normalized();
                }),
                _ => Inhibit(false)
            }
        },

        MotionNotify => {
            Inhibit(true)
        },

        _ => Inhibit(false)
    }
}

fn thread_work(
    img: Image,
    scene: Arc<RwLock<Scene>>,
    max_depth: usize,
) -> ! {
    let mut rng = thread_rng();
    loop {
        for y in 0 .. img.height - 1 {
            for x in 0 .. img.width - 1 {
                let scene_lock = scene.read();
                let u = (x as f32 + rng.gen::<f32>()) / (img.width - 1) as f32;
                let v = ((img.height - y as usize) as f32 + rng.gen::<f32>())
                    / (img.height - 1) as f32;
                let r = scene_lock.camera.get_ray(u, v);
                let pixel_color = r.color(scene_lock.world.clone(), max_depth);
                {
                    let mut lock = img.buf[y].write();
                    let (c, s) = lock[x];
                    lock[x] = (c + pixel_color, s + 1);
                }
            }
        }
    }
}

impl Image {
    fn new(width: usize, height: usize) -> Self {
        Self {
            buf: Arc::new(
                (0..height)
                    .map(|_| RwLock::new(
                        vec![(Color::new(0.0, 0.0, 0.0), 0); width]
                    ))
                    .collect()
            ),
            width,
            height,
        }
    }

    fn clear(&self) {
        for l in self.buf.iter() {
            l.write().fill((Color::new(0.0, 0.0, 0.0), 0));
        }
    }
}
