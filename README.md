# ray

## Compilation

Dépendances :
- [Rust toolchain](https://rustup.rs)

### cli

```
cargo build --package raycli --release
```

### gui

Dépendances :
- gtk+3
- pkg-config

```
cargo build --package raygui --release
```

Les exécutables se trouvent dans `target/release`.

Pour compiler les deux versions :
```
cargo build --release
```

## Utilisation

Se référer à :
```
raycli --help
```

### Exemple

```
raycli scene.yaml -h 1200 -s 50
```
