use anyhow::{
    Context,
    Result,
};
use clap::{
    App,
    Arg,
};
use image::{
    ImageBuffer,
    Rgb,
};
use indicatif::{
    HumanDuration,
    ProgressBar,
    ProgressStyle,
};
use rand::prelude::*;
use rayon::prelude::*;
use std::{
    convert::TryInto,
    time::Instant,
};

use libray::{
    vec::Color,
    scene::Scene,
};

fn main() -> Result<()> {
    let matches = App::new(clap::crate_name!())
        .version(clap::crate_version!())
        .author(clap::crate_authors!())
        .about(clap::crate_description!())
        .arg(
            Arg::with_name("SCENE")
                .help("The YAML scene file")
                .required(true),
        )
        .arg(
            Arg::with_name("output_file")
                .short("o")
                .long("output")
                .takes_value(true)
                .value_name("PATH")
                .help("The output image"),
        )
        .arg(
            Arg::with_name("image_height")
                .short("h")
                .long("height")
                .takes_value(true)
                .value_name("HEIGHT")
                .default_value("360")
                .help("The height in pixels of the image"),
        )
        .arg(
            Arg::with_name("samples_per_pixel")
                .short("s")
                .long("samples")
                .takes_value(true)
                .value_name("SAMPLES")
                .default_value("10")
                .help("The number of samples per pixel"),
        )
        .arg(
            Arg::with_name("max_depth")
                .short("d")
                .long("depth")
                .takes_value(true)
                .value_name("DEPTH")
                .default_value("50")
                .help("The number of times a ray can bounce"),
        )
        .get_matches();

    let image_height: usize = matches.value_of("image_height").unwrap().parse()?;
    let samples_per_pixel: usize = matches.value_of("samples_per_pixel").unwrap().parse()?;
    let max_depth: usize = matches.value_of("max_depth").unwrap().parse()?;

    let scene_file = matches.value_of("SCENE").unwrap();
    let scene_contents = std::fs::read_to_string(scene_file)?;
    let scene: Scene = serde_yaml::from_str(&scene_contents)
        .with_context(|| format!("Failed to read scene from {}", scene_file))?;

    let image_width = (image_height as f32 * scene.camera.aspect_ratio) as usize;

    let mut img: ImageBuffer<Rgb<u8>, _> =
        ImageBuffer::new(image_width.try_into()?, image_height.try_into()?);

    let started = Instant::now();
    let pb = ProgressBar::new(image_height.try_into()?)
        .with_style(ProgressStyle::default_bar().template("Rendering: {wide_bar} (eta: {eta})"));

    // Each chunk is a line of the image.
    // Each line is 3 * image_width u8s long.
    img.par_chunks_exact_mut(3 * image_width)
        .enumerate()
        .for_each(|(y, row)| {
            let mut rng = thread_rng();
            // Each pixel is 3 u8s long.
            for (x, pixel) in row.chunks_exact_mut(3).enumerate() {
                let mut pixel_color = Color::new(0.0, 0.0, 0.0);
                for _ in 0..samples_per_pixel {
                    let u = (x as f32 + rng.gen::<f32>()) / (image_width - 1) as f32;
                    let v = ((image_height - y as usize) as f32 + rng.gen::<f32>())
                        / (image_height - 1) as f32;
                    let r = scene.camera.get_ray(u, v);
                    pixel_color += r.color(scene.world.clone(), max_depth);
                }
                let p = (pixel_color / samples_per_pixel as f32) * 256.0;
                pixel[0] = p.x() as u8;
                pixel[1] = p.y() as u8;
                pixel[2] = p.z() as u8;
            }
            pb.inc(1);
        });

    pb.finish_and_clear();
    println!("Rendered in {}.", HumanDuration(started.elapsed()));

    if let Some(o) = matches.value_of("output_file") {
        img.save(o)?;
        println!("Saved image to {}.", o);
    }

    Ok(())
}
